﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class DialogueExample3 : MonoBehaviour {

    //Give access to the text component.
    public Text dialogueText;

    //The Speed the text is animated on screen. Waits 0.05 seconds before animating the next character.
    //Useful for letting the player accelerate the speed animation.
    public float speedText = 0.05f;

    //Only used in the example below, otherwise you can remove this.
    private string stringToAnimate = "Making a dialogue box in Unity 5. {sound:1}Let's see if this text animation works.";

    //Keep tracks of our commands.
    private List<SpecialCommand> specialCommands;

    void Start() {
        //Example #3
        AnimateDialogueBox(stringToAnimate);
    }

    void Update() {
        //Simple controls to accelerate the text speed.
        if (Input.GetKeyDown(KeyCode.Space)) {
            speedText = speedText / 100;
        } else if (Input.GetKeyUp(KeyCode.Space)) {
            speedText = 0.05f;
        }
    }

    //Call this public function when you want to animate text. This should be used in your other scripts.
    public void AnimateDialogueBox(string text) {
        StartCoroutine(AnimateTextCoroutine(text));
    }

    /*Example #3*/
    //Coroutine for animating the dialogue text. Add line breaks before animating text.
    private IEnumerator AnimateTextCoroutine(string text) {

        dialogueText.text = "";
        int i = 0;

        //Add Line breaks. Important to put this here, before getting the commands or else they won't execute at the right moment.
        text = AddLineBreaks(text);

        specialCommands = BuildSpecialCommandList(text);
        string stripText = StripAllCommands(text);

        while (i < stripText.Length) {

            if (specialCommands.Count > 0) {
                CheckForCommands(i);
            }

            dialogueText.text += stripText[i];

            i++;
            yield return new WaitForSeconds(speedText);
        }

        Debug.Log("Done animating!");
    }


    /*
     *  FUNCTIONS used for getting and executing our special commands in our dialogue line.
     */

    //We use regex to strip all {commands} from our current dialogue line.
    //We have two strings: one with commands and the one printing on screen.
    //We keep track of both in order to know when there's a command to execute, if any.
    private string StripAllCommands(string text) {
        //Clean string to return.
        string cleanString;

        //Regex Pattern. Remove all "{stuff:value}" from our dialogue line.
        string pattern = "\\{.[^}]+\\}";

        cleanString = Regex.Replace(text, pattern, "");
        return cleanString;
    }

    //Build a list containing all our commands.
    private List<SpecialCommand> BuildSpecialCommandList(string text) {

        List<SpecialCommand> listCommand = new List<SpecialCommand>();

        string command = "";                //Current command name
        char[] squiggles = { '{', '}' };    //Trim these characters when the command is found.

        //Go through the dialogue line, get all our special commands.
        for (int i = 0; i < text.Length; i++) {
            string currentChar = text[i].ToString();

            //If true, we are getting a command.
            if (currentChar == "{") {

                //Go ahead and get the command.
                while (currentChar != "}" && i < text.Length) {
                    currentChar = text[i].ToString();
                    command += currentChar;
                    text = text.Remove(i, 1);  //Remove current character. We want to get the next character in the command.
                }

                //Done getting the command.
                if (currentChar == "}") {
                    //Trim "{" and "}"
                    command = command.Trim(squiggles);
                    //Get Command Name and Value.
                    SpecialCommand newCommand = GetSpecialCommand(command);
                    //Command index position in the string.
                    newCommand.Index = i;
                    //Add to list.
                    listCommand.Add(newCommand);
                    //Reset
                    command = "";

                    //Take a step back otherwise a character will be skipped. 
                    //i = 0 also works, but it means going through characters we already checked.
                    i--;
                } else {
                    Debug.Log("Command in dialogue line not closed.");
                }
            }
        }

        return listCommand;
    }

    //Since our command is {command:value}, we want to extract the name of the command and its value.
    private SpecialCommand GetSpecialCommand(string text) {

        SpecialCommand newCommand = new SpecialCommand();

        //Regex to get the command name and the command value
        string commandRegex = "[:]";

        //Split the command and its values.
        string[] matches = Regex.Split(text, commandRegex);

        //Get the command and its values.
        if (matches.Length > 0) {
            for (int i = 0; i < matches.Length; i++) {
                //0 = command name. 1 >= value.
                if (i == 0) {
                    newCommand.Name = matches[i];
                } else {
                    newCommand.Values.Add(matches[i]);
                }
            }
        } else {
            //Oh no....
            return null;
        }

        return newCommand;
    }

    //Check all commands in a given index. 
    //It's possible to have two commands next to each other in the dialogue line.
    //This means both will share the same index.
    private void CheckForCommands(int index) {
        for (int i = 0; i < specialCommands.Count; i++) {
            if (specialCommands[i].Index == index) {
                //Execute if found a match.
                ExecuteCommand(specialCommands[i]);

                //Remove it.
                specialCommands.RemoveAt(i);

                //Take a step back since we removed one command from the list. Otherwise, the script will skip one command.
                i--;
            }
        }
    }

    //Where you will execute your command!
    private void ExecuteCommand(SpecialCommand command) {
        if (command == null) {
            return;
        }

        Debug.Log("Command " + command.Name + " found!");

        if (command.Name == "sound") {
            Debug.Log("BOOOOM! Command played a sound.");
        }
    }

    /*
     *  Manual Line Breaks. (You don't need this for your game. It's only used for the 3rd example.)
     *
     */

    private string AddLineBreaks(string text) {

        string rebuiltString = "";          //Rebuilt dialogue with line breaks.
        bool isPrintingCommand = false;     //Ignore commands
        float lineWidth = 0;                //Current width of the current line being calculated.
        float maxWidthBeforeBreak = 200;    //Max width before line break. Up to you.
        Font myFont = dialogueText.font;    //We need the font.

        //Need this to calculate each letters / characters' width.
        CharacterInfo characterInfo = new CharacterInfo();  //Not related to Characters as in actors in the scene. This deals with actual letters.

        //Create an array containing all characters.
        char[] characterArray = text.ToCharArray();

        //Current char
        string currentChar = "";

        //Last empty space index. This is where we will put our line break.
        int lastSpaceCharIndex = 0;

        //Contain all line breaks to add to our current string.
        List<int> lineBreaksList = new List<int>();

        //Go through
        for (int i = 0; i < characterArray.Length; i++) {
            currentChar = characterArray[i].ToString();

            if (currentChar == " ") {
                //Keep in memory the last space so we can actually add the \n.
                lastSpaceCharIndex = i;
            }

            //When printing a special command, don't calculate the width.
            if (!isPrintingCommand && currentChar == "{") {
                isPrintingCommand = true;
            } else if (currentChar == "}") {
                isPrintingCommand = false;
                continue;
            }

            //Don't calculate width if the current character is part of a special command.
            if (isPrintingCommand) {
                continue;
            }

            //Calculate width.
            myFont.GetCharacterInfo(characterArray[i], out characterInfo, dialogueText.fontSize);
            lineWidth += characterInfo.advance;

            //If we reached the maximum width, add the last space index to a list to be used later.
            if (lineWidth >= maxWidthBeforeBreak) {
                lineWidth = 0;
                lineBreaksList.Add(lastSpaceCharIndex);
            }
        }

        //Rebuild the string and add all the line breaks at the right position.
        int lineBreaksListCounter = 0;
        for (int i = 0; i < characterArray.Length; i++) {

            rebuiltString += characterArray[i].ToString();

            //Check if the current index needs a enter after.
            if (lineBreaksListCounter < lineBreaksList.Count) {
                if (lineBreaksList[lineBreaksListCounter] == i) {
                    rebuiltString += " \n";
                    lineBreaksListCounter++;
                }
            }

        }

        return rebuiltString;
    }
}


//Basic Class for our special command.
class SpecialCommand
{

    //Command Name
    public string Name { get; set; }

    //A list of all values needed for our command. 
    //If you only need one value per command, consider not making this a list.
    public List<string> Values { get; set; }

    //Which character index should we execute this command.
    public int Index { get; set; }

    public SpecialCommand()
    {
        Name = "";
        Values = new List<string>();
        Index = 0;
    }
}
