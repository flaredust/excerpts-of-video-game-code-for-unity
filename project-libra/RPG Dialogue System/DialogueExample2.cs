﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class DialogueExample2 : MonoBehaviour {

    //Give access to the text component.
    public Text dialogueText;

    //The Speed the text is animated on screen. Waits 0.05 seconds before animating the next character.
    //Useful for letting the player accelerate the speed animation.
    public float speedText = 0.05f;

    //Only used in the example below, otherwise you can remove this.
    private string stringToAnimate = "Making a {sound:1}dialogue box in Unity 5. {color:red}Let's see{color:end} if this text animation works.";

    //Keep tracks of our commands.
    private List<SpecialCommand> specialCommands;

    void Start() {
        //Example #2
        AnimateDialogueBox(stringToAnimate);
    }

    void Update() {
        //Simple controls to accelerate the text speed.
        if (Input.GetKeyDown(KeyCode.Space)) {
            speedText = speedText / 100;
        } else if (Input.GetKeyUp(KeyCode.Space)) {
            speedText = 0.05f;
        }
    }

    //Call this public function when you want to animate text. This should be used in your other scripts.
    public void AnimateDialogueBox(string text) {
        StartCoroutine(AnimateTextCoroutine(text));
    }

    /*Example #2*/
    //Coroutine for animating the dialogue text.
    //Special Commands Available!
    private IEnumerator AnimateTextCoroutine(string text) {

        dialogueText.text = "";
        int i = 0;

        //Get all special commands in the current text before printing it. Hold our SpecialCommand in a dictionary.
        specialCommands = BuildSpecialCommandList(text);

        //Strip all commands from the text, so we can print it on the screen.
        string stripText = StripAllCommands(text);

        while (i < stripText.Length) {

            //Check if we have commands in the current dialogue line.. If so, execute it.
            if (specialCommands.Count > 0) {
                CheckForCommands(i);    //feed it the current character index.
            }

            //Use the text with no commands in it.
            dialogueText.text += stripText[i];

            i++;
            yield return new WaitForSeconds(speedText);
        }

        Debug.Log("Done animating!");
    }


    /*
     *  FUNCTIONS used for getting and executing our special commands in our dialogue line.
     */

    //We use regex to strip all {commands} from our current dialogue line.
    //We have two strings: one with commands and the one printing on screen.
    //We keep track of both in order to know when there's a command to execute, if any.
    private string StripAllCommands(string text) {
        //Clean string to return.
        string cleanString;

        //Regex Pattern. Remove all "{stuff:value}" from our dialogue line.
        string pattern = "\\{.[^}]+\\}";

        cleanString = Regex.Replace(text, pattern, "");
        return cleanString;
    }

    //Build a list containing all our commands.
    private List<SpecialCommand> BuildSpecialCommandList(string text) {

        List<SpecialCommand> listCommand = new List<SpecialCommand>();

        string command = "";                //Current command name
        char[] squiggles = { '{', '}' };    //Trim these characters when the command is found.

        //Go through the dialogue line, get all our special commands.
        for (int i = 0; i < text.Length; i++) {
            string currentChar = text[i].ToString();

            //If true, we are getting a command.
            if (currentChar == "{") {

                //Go ahead and get the command.
                while (currentChar != "}" && i < text.Length) {
                    currentChar = text[i].ToString();
                    command += currentChar;
                    text = text.Remove(i, 1);  //Remove current character. We want to get the next character in the command.
                }

                //Done getting the command.
                if (currentChar == "}") {
                    //Trim "{" and "}"
                    command = command.Trim(squiggles);
                    //Get Command Name and Value.
                    SpecialCommand newCommand = GetSpecialCommand(command);
                    //Command index position in the string.
                    newCommand.Index = i;
                    //Add to list.
                    listCommand.Add(newCommand);
                    //Reset
                    command = "";

                    //Take a step back otherwise a character will be skipped. 
                    //i = 0 also works, but it means going through characters we already checked.
                    i--;
                } else {
                    Debug.Log("Command in dialogue line not closed.");
                }
            }
        }

        return listCommand;
    }

    //Since our command is {command:value}, we want to extract the name of the command and its value.
    private SpecialCommand GetSpecialCommand(string text) {

        SpecialCommand newCommand = new SpecialCommand();

        //Regex to get the command name and the command value
        string commandRegex = "[:]";

        //Split the command and its values.
        string[] matches = Regex.Split(text, commandRegex);

        //Get the command and its values.
        if (matches.Length > 0) {
            for (int i = 0; i < matches.Length; i++) {
                //0 = command name. 1 >= value.
                if (i == 0) {
                    newCommand.Name = matches[i];
                } else {
                    newCommand.Values.Add(matches[i]);
                }
            }
        } else {
            //Oh no....
            return null;
        }

        return newCommand;
    }

    //Check all commands in a given index. 
    //It's possible to have two commands next to each other in the dialogue line.
    //This means both will share the same index.
    private void CheckForCommands(int index) {
        for (int i = 0; i < specialCommands.Count; i++) {
            if (specialCommands[i].Index == index) {
                //Execute if found a match.
                ExecuteCommand(specialCommands[i]);

                //Remove it.
                specialCommands.RemoveAt(i);

                //Take a step back since we removed one command from the list. Otherwise, the script will skip one command.
                i--;
            }
        }
    }

    //Where you will execute your command!
    private void ExecuteCommand(SpecialCommand command) {
        if (command == null) {
            return;
        }

        Debug.Log("Command " + command.Name + " found!");

        if (command.Name == "sound") {
            Debug.Log("BOOOOM! Command played a sound.");
        }
    }
}

//Basic Class for our special command.
class SpecialCommand
{

    //Command Name
    public string Name { get; set; }

    //A list of all values needed for our command. 
    //If you only need one value per command, consider not making this a list.
    public List<string> Values { get; set; }

    //Which character index should we execute this command.
    public int Index { get; set; }

    public SpecialCommand()
    {
        Name = "";
        Values = new List<string>();
        Index = 0;
    }
}
